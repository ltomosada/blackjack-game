const suits = ["Spade", "Diamond", "Club", "Heart"];
const values = [
  "A",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K",
];

let deck = [];
let dealer = { name: "dealer", hand: [] };
let player = { name: "player", hand: [] };
const gameSetup = document.getElementById("content").innerHTML;

function createDeck() {
  for (let suit of suits) {
    for (let value of values) {
      let weight = parseInt(value);
      if (value === "J" || value === "Q" || value === "K") {
        weight = 10;
      } else if (value === "A") {
        weight = 11;
      }
      let card = { suit, value, weight };
      deck.push(card);
    }
  }
}

function shuffleDeck() {
  for (let i = 0; i < 1000; i++) {
    let firstIndex = Math.floor(Math.random() * deck.length);
    let secondIndex = Math.floor(Math.random() * deck.length);
    let temp = deck[firstIndex];
    deck[firstIndex] = deck[secondIndex];
    deck[secondIndex] = temp;
  }
}

function dealCards(person, quantity) {
  checkDeck(quantity);
  for (let i = 0; i < quantity; i++) {
    person.hand.push(deck.pop());
  }
}

function displayCards(person, hand) {
  for (let i = 0; i < hand.length; i++) {
    document.getElementById(`${person.name}Value${i}`).innerText =
      hand[i].value;
    document.getElementById(`${person.name}Suit${i}`).innerText = hand[i].suit;
  }
}

function sumScore(person) {
  return person.hand.reduce((sum, card) => sum + card.weight, 0);
}

function showScore(person) {
  document.getElementById(`${person.name}Score`).innerText = sumScore(person);
}

function checkScore() {
  let score = sumScore(player);
  if (score > 21) {
    document.getElementById("winner").innerText = "Bust! Dealer Wins";
    hideButtons();
    showDealerCard();
    showScore(dealer);
  }
}

function calculateWinner() {
  dealerTurn();
  hideButtons();
  const dealerScore = sumScore(dealer);
  const playerScore = sumScore(player);
  let winner = "";
  if ((playerScore > dealerScore && playerScore < 22) || dealerScore > 21) {
    winner = "Player";
  } else if (playerScore === dealerScore) {
    winner = "Push, no one";
  } else {
    winner = "Dealer";
  }
  document.getElementById("winner").innerText = `${winner} wins`;
}

function checkDeck(needed) {
  if (deck.length < needed) {
    deck = [];
    createDeck();
    shuffleDeck();
  }
}

function checkBlackjack(person) {
  if (sumScore(person) === 21) {
    document.getElementById(
      "winner"
    ).innerText = `Blackjack, ${person.name} wins!`;
    showDealerCard();
    showScore(dealer);
  }
}

function resetGame() {
  player.hand = [];
  dealer.hand = [];
  hit.style.visibility = "visible";
  stand.style.visibility = "visible";
  document.getElementById("content").innerHTML = gameSetup;
  dealCards(dealer, 2);
  dealCards(player, 2);
  displayCards(player, player.hand);
  displayCards(dealer, [dealer.hand[0]]);
  showScore(player);
  checkBlackjack(dealer);
  checkBlackjack(player);
}

function hitMe() {
  dealCards(player, 1);
  createCard(player);
  showScore(player);
  checkScore();
}

function createCard(person) {
  const newCard = document.createElement("div");
  newCard.classList.add("card");
  const value = document.createElement("div");
  value.classList.add("value");
  value.setAttribute("id", `${person.name}Value${person.hand.length - 1}`);
  const suit = document.createElement("div");
  suit.setAttribute("id", `${person.name}Suit${person.hand.length - 1}`);
  value.innerText = person.hand[person.hand.length - 1].value;
  suit.innerText = person.hand[person.hand.length - 1].suit;
  newCard.appendChild(value);
  newCard.appendChild(suit);
  document.getElementById(`${person.name}Cards`).appendChild(newCard);
}

function hideButtons() {
  hit.style.visibility = "hidden";
  stand.style.visibility = "hidden";
}

function dealerTurn() {
  showDealerCard();
  while (sumScore(dealer) < 17) {
    dealCards(dealer, 1);
    createCard(dealer);
  }
  showScore(dealer);
  displayCards(dealer, dealer.hand);
}

function showDealerCard() {
  document.getElementById("dealerCard1").classList.remove("hiddenCard");
  document.getElementById("dealerCard1").classList.add("card");
  document.getElementById("dealerValue1").innerText =
    dealer.hand[dealer.hand.length - 1].value;
  document.getElementById("dealerSuit1").innerText =
    dealer.hand[dealer.hand.length - 1].suit;
}

createDeck();
shuffleDeck();
dealCards(dealer, 2);
dealCards(player, 2);
displayCards(player, player.hand);
displayCards(dealer, [dealer.hand[0]]);
showScore(player);

const hit = document.getElementById("hit");
const stand = document.getElementById("stand");
const newGame = document.getElementById("newGame");

stand.addEventListener("click", calculateWinner);
hit.addEventListener("click", hitMe);
newGame.addEventListener("click", resetGame);
